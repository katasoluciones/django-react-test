# This is a Django and ReactJS test project

In this Repo we are doing a test app with the stack Django and ReactJS

- [This is a Django and ReactJS test project](#this-is-a-django-and-reactjs-test-project)
  - [Run server](#run-server)
  - [API](#api)
  - [Websockets](#websockets)

## Run server

To begin, you will need to have working this project in you local environment.

```bash
# You need to have previously installed a venv
pip install -r requirements.txt
./manage.py runserver
./manage.py createsuperuser --username=admin
  # Input an email address
  # Input a password
```

## API

You can enter to the API with <http://localhost:8000/api/v1/notes/>

## Websockets

Is important to download:

  1. [Docker](https://hub.docker.com/search/?type=edition&offering=community)
  2. [Redis](https://hub.docker.com/_/redis/)

Once downloaded and installed **Docker**, now in the Terminal:

```bash
docker run -p 6379:6379 -d redis:4
docker ps # To see the active containers
```