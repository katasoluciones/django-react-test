// Dependencies
import React from 'react';

// Components
import Dashboard from './components/Dashboard';


const Main = props => (
  <div>
    <Dashboard {...props} />
  </div>
);

export default Main;
