// Dependencies
import React from 'react';
import PropTypes from 'prop-types';

// Material-UI
import { withStyles, createStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';

const styles = createStyles({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: 8
  }
});


const EditNoteForm = ({ classes, note, handleNoteOnChange }) => (
  <form className={classes.form}>
    <Typography variant="h5" noWrap>
      {note.title}
    </Typography>
    <FormControl margin="normal" required={false} fullWidth>
      <InputLabel htmlFor="content">Content</InputLabel>
      <Input
        type="textarea"
        name="content"
        rowsMax="4"
        id="content"
        multiline
        autoComplete="current-content"
        value={note.content}
        onChange={handleNoteOnChange}
      />
    </FormControl>
  </form>
);

EditNoteForm.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  note: PropTypes.objectOf(PropTypes.any),
  handleNoteOnChange: PropTypes.func
};

export default withStyles(styles)(EditNoteForm);
