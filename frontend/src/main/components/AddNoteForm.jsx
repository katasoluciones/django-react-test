// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Material-UI
import { withStyles, createStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

const styles = createStyles({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: 8
  },
  submit: {
    marginTop: 8 * 3
  }
});


class AddNoteForm extends Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.any).isRequired,
    handleSaveNote: PropTypes.func
  };

  state = {
    title: '',
    content: ''
  }

  handleOnSubmit = e => {
    const { handleSaveNote } = this.props;
    e.preventDefault();

    handleSaveNote(this.state);
    // Clear the inputs
    this.setState({
      title: '',
      content: ''
    });
  }

  handleInputOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    const { classes } = this.props;
    const { title, content } = this.state;
    return (
      <>
        <form className={classes.form} onSubmit={this.handleOnSubmit} noValidate autoComplete="off">
          <FormControl margin="normal" required={false} fullWidth>
            <InputLabel htmlFor="title">Title</InputLabel>
            <Input
              id="title"
              name="title"
              autoComplete="title"
              autoFocus
              variant="outlined"
              value={title}
              onChange={this.handleInputOnChange}
            />
          </FormControl>
          <FormControl margin="normal" required={false} fullWidth>
            <InputLabel htmlFor="content">Content</InputLabel>
            <Input
              name="content"
              rowsMax="4"
              id="content"
              multiline
              autoComplete="current-content"
              value={content}
              onChange={this.handleInputOnChange}
            />
          </FormControl>
          <Button
            type="submit"
            // fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Create Note
          </Button>
        </form>
      </>
    );
  }
}

export default withStyles(styles)(AddNoteForm);
