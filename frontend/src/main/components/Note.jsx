// Dependencies
import React from 'react';
import PropTypes from 'prop-types';

// Material-UI
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const styles = createStyles({
  listContainer: {
    maxWidth: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  titleStyles: {
    width: '100%',
    fontFamily: 'Roboto',
    fontSize: 21,
    fontWeight: 500
  },
  contentStyles: {
    width: '100%',
    fontFamily: 'Roboto',
    fontSize: 15,
    fontWeight: 300
  }
});

const Note = ({ classes, title, content }) => (
  <div className={classes.listContainer}>
    <Typography variant="h5" noWrap className={classes.titleStyles}>
      {title}
    </Typography>
    <Typography variant="h6" noWrap className={classes.contentStyles}>
      {content}
    </Typography>
  </div>
);

Note.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  title: PropTypes.string,
  content: PropTypes.string
};

export default withStyles(styles)(Note);
