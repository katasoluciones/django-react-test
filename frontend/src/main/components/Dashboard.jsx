import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Websocket from 'react-websocket';

// Material-UI Components
import Button from '@material-ui/core/Button';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

// Components
import ListNotes from './ListNotes';
import AddNoteForm from './AddNoteForm';
import EditNoteForm from './EditNoteForm';

// API
import { fetchNotes, fetchNote, updateNote, addNote } from '../../api'; // eslint-disable-line

const styles = createStyles({
  centerButton: {
    textAlign: 'center'
  },
  formStyles: {
    width: '80%'
  }
});


class Dashboard extends Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      notes: [],
      note: {},
      currentNoteId: 0,
      isCreating: true,
      isFetching: true // eslint-disable-line
    };
  }

  componentDidMount() {
    this.getData();
  }

  timeout = ms => new Promise(res => setTimeout(res, ms));

  handleItemClick = async id => {
    const selectedNote = await fetchNote(id);
    this.setState({
      isCreating: false,
      currentNoteId: id,
      note: selectedNote
    });
  }

  handleAddNote = () => {
    this.setState({
      isCreating: true
    });
  }

  getData = async () => {
    const data = await fetchNotes();
    this.setState({
      notes: data
    });
  }

  handleSaveNote = async data => {
    await addNote(data);
    await this.timeout(1000);
    await this.getData();
  }

  handleSocketData = data => {
    const { note } = this.state;
    const result = JSON.parse(data);

    const currentNote = note;

    if (currentNote.id === result.id) {
      this.setState({
        note: result
      });
    }
  }

  handleNoteOnChange = async e => {
    const { note } = this.state;
    const content = e.target.value;
    const currentNote = note;
    currentNote.content = content;
    console.log('current note: ', currentNote);

    this.setState({
      note: currentNote
    });
    // eslint-disable-next-line react/no-string-refs
    const { socket } = this.refs;
    socket.state.ws.send(JSON.stringify(currentNote));

    await this.timeout(1000);
    await this.getData();
  }

  render() {
    const { classes } = this.props;
    const {
      notes, note, currentNoteId, isCreating
    } = this.state;
    return (
      <>
        <Grid container spacing={0}>
          <Grid item xs={2} />
          <Grid item xs={6}>
            <Typography variant="h5" noWrap>
              {`Realtime notes for current note ${currentNoteId}`}
            </Typography>
          </Grid>
          <Grid item xs={2} className={classes.centerButton}>
            <Button variant="contained" color="primary" onClick={this.handleAddNote}>
              Create a new Note
            </Button>
          </Grid>
          <Grid item xs={2} />
        </Grid>
        <Grid container spacing={16}>
          <Grid item xs={2} />
          <Grid item xs={3}>
            <ListNotes
              notes={notes}
              handleItemClick={this.handleItemClick}
            />
          </Grid>
          <Grid item xs={5} className={classes.formStyles}>
            {isCreating
              ? <AddNoteForm handleSaveNote={this.handleSaveNote} />
              : <EditNoteForm note={note} handleNoteOnChange={this.handleNoteOnChange} />
            }
            <Websocket
              // eslint-disable-next-line react/no-string-refs
              ref="socket"
              url="ws://127.0.0.1:8000/ws/notes"
              onMessage={this.handleSocketData}
            />
          </Grid>
          <Grid item xs={2} />
        </Grid>
      </>
    );
  }
}

export default withStyles(styles)(Dashboard);
