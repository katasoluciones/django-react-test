// Dependencies
import React from 'react';
import PropTypes from 'prop-types';

// Material-UI
import { withStyles, createStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';

// Components
import Note from './Note';

const styles = createStyles({
  listContainer: {
    width: '100%',
    backgroundColor: 'white',
  },
  fontFamilyNotes: {
    fontFamily: 'Roboto'
  }
});


const ListNotes = ({ classes, notes, handleItemClick }) => (
  <div className={classes.listContainer}>
    <Grid container spacing={0}>
      <Grid item xs={12}>
        <List component="nav" href="#">
          {notes && notes.map(note => (
            <ListItem
              key={note.id}
              button
              onClick={id => handleItemClick(note.id)}
              className={classes.fontFamilyNotes}
            >
              <Note
                title={note.title}
                content={note.content}
              />
            </ListItem>
          ))}
        </List>
      </Grid>
    </Grid>
  </div>
);

ListNotes.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  notes: PropTypes.arrayOf(PropTypes.object),
  handleItemClick: PropTypes.func
};

export default withStyles(styles)(ListNotes);
