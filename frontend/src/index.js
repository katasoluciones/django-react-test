/* eslint react/jsx-filename-extension: "off" */
// Dependencies
import React from 'react';
import { render } from 'react-dom';

// Container
import App from './App';

const appElement = document.getElementById('root');

// App Wrapper
const renderApp = Component => {
  render(
    <Component />,
    appElement
  );
};

// Rendering App
renderApp(App);
