// Dependencies
import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

// Components
import Main from './main';

// Material UI themes
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});


export default (props) => (
  <MuiThemeProvider theme={theme}>
    <div>
      <Main />
    </div>
  </MuiThemeProvider>
);
