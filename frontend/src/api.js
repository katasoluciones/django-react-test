// Dependencies
import fetch from 'node-fetch';

const url = 'http://127.0.0.1:8000/api/v1/notes/';

export const fetchNotes = async () => (
  fetch(url, {})
    .then(res => res.json())
    .then(data => data)
);

export const fetchNote = (id) => (
  fetch(`${url}${id}`, {})
    .then(res => res.json())
    .then(data => (data))
);

export const addNote = note => {
  fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(note)
  })
    .then(res => res.json())
    .then(data => {
      console.log('data: ', data);
    });

  return note;
};

export const updateNote = note => {
  console.log('We are updating...');
  console.log('Update a note with id: ', note.id);
};
